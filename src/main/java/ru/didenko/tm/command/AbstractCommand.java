package ru.didenko.tm.command;

import lombok.NoArgsConstructor;
import ru.didenko.tm.boostrap.Boostrap;
import ru.didenko.tm.api.service.ServiceLocator;
import ru.didenko.tm.service.TerminalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

@NoArgsConstructor
public abstract class AbstractCommand<T extends AbstractCommand<T>> {
    protected ServiceLocator serviceLocator;
    protected TerminalService terminalService;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.terminalService = serviceLocator.getTerminalService();
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException;

    public abstract boolean isSecure();

}
