package ru.didenko.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.util.Assertion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand<TaskListCommand> {

    @Override
    public final String getName() {
        return "task-list";
    }

    @Override
    public final String getDescription() {
        return "Show all tasks";
    }

    @Override
    public final void execute() throws IOException {
        System.out.println("[TASK LIST]");
        @NotNull int index = 1;
        System.out.println("SHOW TASKS BY: START, FINISH, CREATION, STATUS?");
        final @NotNull String choice = terminalService.nextLine();
        List<Task> tasks = null;
        switch (choice) {
            case "start":
                tasks = serviceLocator.getTaskService().sortByStart();
                break;
            case "finish":
                tasks = serviceLocator.getTaskService().sortByFinish();
                break;
            case "creation":
                tasks = serviceLocator.getTaskService().sortByCreation();
                break;
            case "status":
                tasks = serviceLocator.getTaskService().sortByStatus();
                break;
            default:
                System.err.println("REPEAT PLZ");
                execute();
                break;
        }
        if (!Assertion.assertNotNull(tasks)) return;
        for (@NotNull Task task : tasks) {
            if (serviceLocator.getUserService().getCurrentUser().getRole() == Role.USER) {
                if (task.getUserId().equals(serviceLocator.getUserService().getCurrentUser().getRole())) {
                    System.out.println(index++ + ". " + task.getName());
                }
            }
            System.out.println(index++ + ". " + task.getName());
        }
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
