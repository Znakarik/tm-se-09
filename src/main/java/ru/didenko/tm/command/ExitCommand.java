package ru.didenko.tm.command;

import lombok.NoArgsConstructor;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class ExitCommand extends AbstractCommand<ExitCommand> {

    @Override
    public final String getName() {
        return "exit";
    }

    @Override
    public final String getDescription() {
        return "Exit task manager";
    }

    @Override
    public final void execute() throws IOException, ParseException {
        System.exit(0);
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
