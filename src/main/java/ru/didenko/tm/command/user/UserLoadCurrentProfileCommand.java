package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class UserLoadCurrentProfileCommand extends AbstractCommand<UserLoadCurrentProfileCommand> {

    @Override
    public final String getName() {
        return "user-current";
    }

    @Override
    public final String getDescription() {
        return "Get current profile";
    }

    @Override
    public final void execute() throws IOException, ParseException {
        System.out.println(serviceLocator.getUserService().getCurrentUser());
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
