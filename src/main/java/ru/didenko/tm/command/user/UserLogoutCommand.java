package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import ru.didenko.tm.command.AbstractCommand;

import java.io.IOException;
import java.text.ParseException;

@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand<UserLogoutCommand> {

    @Override
    public final String getName() {
        return "user-logout";
    }

    @Override
    public final String getDescription() {
        return "Log out from task manager";
    }

    @Override
    public final void execute() throws IOException, ParseException {
        System.out.println("[LOG OUT FROM TASK MANAGER]");
        serviceLocator.getUserService().setCurrentUser(null);
        System.out.println("YOU'RE LOGGED OUT");
    }

    @Override
    public final boolean isSecure() {
        return false;
    }
}
