package ru.didenko.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.entity.User;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

@NoArgsConstructor
public final class UserClearCommand extends AbstractCommand<UserClearCommand> {

    @Override
    public final String getName() {
        return "user-clear";
    }

    @Override
    public final String getDescription() {
        return "Delete all users";
    }

    @Override
    public final void execute() throws IOException, ParseException {
        @NotNull final Iterator<User> iterator = serviceLocator.getUserService().findAll().iterator();
        System.out.println("[REMOVE ALL USERS]");
        while (iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
        if (serviceLocator.getUserService().findAll().size() == 0) {
            System.out.println("[ALL USERS DELETED]");
        }
    }

    @Override
    public final boolean isSecure() {
        return true;
    }
}
