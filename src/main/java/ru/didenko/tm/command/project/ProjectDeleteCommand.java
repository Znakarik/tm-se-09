package ru.didenko.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.util.Assertion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

@NoArgsConstructor
public final class ProjectDeleteCommand extends AbstractCommand<ProjectDeleteCommand> {

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER NAME");
        final String projectName = serviceLocator.getTerminalService().nextLine();
        if (serviceLocator.getUserService().getCurrentUser().getRole() == Role.ADMIN) {
            Project project = serviceLocator.getProjectService().findOneByName(projectName);
            if (serviceLocator.getTaskService().findAll().size() != 0) {
                serviceLocator.getTaskService().findAll().removeIf(task -> task.getProjectId().equals(project.getId()));
                serviceLocator.getProjectService().delete(projectName);
            } else {
                serviceLocator.getProjectService().delete(projectName);
            }
        } else {
            // Удаление проекта и подтасков для юзера (юзер может удалить только свои проекты)
            Project project = serviceLocator.getProjectService().secureGetOne(serviceLocator.getUserService().getCurrentUser().getId(), projectName);
            Iterator<Project> projectIterator = serviceLocator.getProjectService().findAll().iterator();
            Iterator<Task> taskIterator = serviceLocator.getTaskService().findAll().iterator();
            while (projectIterator.hasNext()) {
                Project projectFromRepo = projectIterator.next();
                if (project.getUserId() != null) {
                    if (serviceLocator.getTaskService().findAll().size() != 0) {
                        Task task = taskIterator.next();
                        if (!Assertion.assertNotNull(task.getProjectId()) && task.getProjectId().equals(projectFromRepo.getId())) {
                            taskIterator.remove();
                        }
                    }
                    projectIterator.remove();
                }
            }
        }
        System.out.println("[PROJECT DELETED]");
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
