package ru.didenko.tm.command;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public class SaveCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "save";
    }

    @Override
    public String getDescription() {
        return "Save data";
    }

    @Override
    public void execute() throws IOException, ParseException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException {

    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
