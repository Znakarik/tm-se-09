package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.util.HasherPassword;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.security.NoSuchAlgorithmException;

@XmlType(name = "user")
@XmlRootElement
@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {
    @XmlElement(name = "pass")
    @Nullable
    private String password;
    @XmlElement(name = "role")
    @Nullable
    private Role role;

    public User(@NotNull String name,
                @NotNull String password,
                @NotNull Role role) throws NoSuchAlgorithmException {
        this.name = name;
        this.password = HasherPassword.fromStringToHash(password);
        this.role = role;
    }

    @NotNull
    @Override
    public String toString() {
        return "[USER]" +
                "LOGIN:" + name + '\'' +
                ", ROLE: " + role +
                ", ID: " + id + '\'';
    }
}
