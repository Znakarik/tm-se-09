package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Status;

import javax.xml.bind.annotation.*;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Project.class, Task.class, User.class})
public abstract class AbstractEntity {

    @XmlElement(name = "id")
    @NotNull
    String id = UUID.randomUUID().toString();
    @XmlElement(name = "name")
    @Nullable
    String name;
    @XmlElement(name = "creation_date")
    @NotNull Date creation = new Date();

    @Override
    public String toString() {
        return "AbstractEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
