package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Status;
import ru.didenko.tm.util.DateParser;

import javax.xml.bind.annotation.*;
import java.util.*;


@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "project")
public final class Project extends AbstractGoal {

    @XmlElement(name = "description")
    @Nullable
    String description;
    @XmlElement(name = "dateStart")
    @Nullable
    Date dateStart;
    @XmlElement(name = "dateFinish")
    @Nullable
    Date dateFinish;
    @XmlElement(name = "userId")
    @Nullable
    String userId;
    @XmlElement(name = "status")
    @Nullable
    Status status;

    public Project(@NotNull String name,
                   @Nullable String description,
                   @Nullable Date dateStart,
                   @Nullable Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        status = Status.PLANNED;
    }

    @Nullable
    @lombok.SneakyThrows
    @Override
    public String toString() {
        return "***\n[PROJECT]\nNAME: " + name + '\n' +
                "ID: " + id +
                "\n DESCRIPTION: " + description +
                "\n START DATE: " + DateParser.fromDateToString(dateStart) +
                "\n FINISH DATE: " + DateParser.fromDateToString(dateFinish);
    }
}

