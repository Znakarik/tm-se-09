package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Status;

import javax.xml.bind.annotation.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Project.class, Task.class})
public abstract class AbstractGoal extends AbstractEntity {

    @XmlElement(name = "description")
    @Nullable
    String description;
    @XmlElement(name = "dateStart")
    @Nullable
    Date dateStart;
    @XmlElement(name = "dateFinish")
    @Nullable
    Date dateFinish;
    @XmlElement(name = "userId")
    @Nullable
    String userId;
    @XmlElement(name = "status")
    @Nullable
    Status status;

}
