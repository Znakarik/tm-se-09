package ru.didenko.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Status;
import ru.didenko.tm.util.DateParser;

import javax.xml.bind.annotation.*;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "task")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Task extends AbstractGoal {

    @XmlElement(name = "description")
    @Nullable
    String description;
    @XmlElement(name = "dateStart")
    @Nullable
    Date dateStart;
    @XmlElement(name = "dateFinish")
    @Nullable
    Date dateFinish;
    @XmlElement(name = "userId")
    @Nullable
    String userId;
    @XmlElement(name = "status")
    @Nullable
    Status status;
    @XmlElement(name = "projectId")
    @Nullable
    private String projectId;

    public Task(@NotNull String name,
                @Nullable String description,
                @Nullable Date dateStart,
                @Nullable Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        status = Status.PLANNED;
    }

    @NotNull
    @Override
    public String toString() {
        return " TASK: " + '\n' +
                "STATUS: " + status + "\n" +
                "ID: " + id + '\n' +
                "NAME: " + name + '\n' +
                "DESCRIPTION: " + description + '\n' +
                "START DATE: " + DateParser.fromDateToString(dateStart) + '\n' +
                "FINISH DATE: " + DateParser.fromDateToString(dateFinish) + '\n' +
                "PROJECT ID: " + projectId;
    }
}
