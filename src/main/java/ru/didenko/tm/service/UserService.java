package ru.didenko.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.repository.UserRepository;
import ru.didenko.tm.api.user.UserServiceInterface;
import ru.didenko.tm.util.Assertion;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class UserService extends AbstractService<User> implements UserServiceInterface {

    @NotNull
//    private UserRepository userRepository;//= (UserRepository) abstractRepository;
    @Nullable
    private User currentUser;

    public UserService(final @Nullable UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public void createUser(final @NotNull String login,
                           final @NotNull String pass,
                           final @NotNull Role role) throws NoSuchAlgorithmException {
        if (!Assertion.assertNotNull(login, pass, role)) return;
        abstractRepository.persist(new User(login, pass, role));
    }

    @Override
    public void setCurrentUserByName(final @NotNull String name) {
        if (!Assertion.assertNotNull(name)) return;
        currentUser = abstractRepository.findOneByName(name);
    }

    @Nullable
    @Override
    public User findUser(final @NotNull String login, final @NotNull String pass) {
        if (!Assertion.assertNotNull(login, pass)) return null;
        User user = abstractRepository.findOneByName(login);
        String userPass = user != null ? user.getPassword() : null;
        assert userPass != null;
        if (userPass.equals(pass)) {
            return abstractRepository.findOneByName(login);
        }
        return null;
    }

    @Override
    public List<User> getList() {
        return new ArrayList<>(findAll());
    }
}
