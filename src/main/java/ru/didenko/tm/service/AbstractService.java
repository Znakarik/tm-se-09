package ru.didenko.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.AbstractServiceInterface;
import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.repository.AbstractRepository;
import ru.didenko.tm.util.Assertion;

import java.util.Collection;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements AbstractServiceInterface<T> {
    @NotNull
    AbstractRepository<T> abstractRepository;

    public AbstractService(@NotNull final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @NotNull
    @Override
    public Collection<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }

    @Override
    public void showAll() {
        System.out.println(abstractRepository.findAll().toString());
    }

    @Nullable
    public T findOneByName(@NotNull String name) {
        if (!Assertion.assertNotNull(name)) return null;
        return abstractRepository.findOneByName(name);
    }

    public void delete(@NotNull String name) {
        if (!Assertion.assertNotNull(name)) return;
        T entity = abstractRepository.findOneByName(name);
        String id = entity.getId();
        abstractRepository.remove(id);
    }

}
