package ru.didenko.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.GoalServiceInterface;
import ru.didenko.tm.constant.Status;
import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.entity.AbstractGoal;
import ru.didenko.tm.repository.AbstractRepository;
import ru.didenko.tm.repository.ProjectRepository;
import ru.didenko.tm.util.Assertion;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractGoalService<T extends AbstractGoal> extends AbstractService<T> implements GoalServiceInterface<T> {

    public AbstractGoalService(@NotNull final AbstractRepository<T> abstractRepository) {
        super(abstractRepository);
    }

    @Nullable
    public final T search(@NotNull final String searchVal) {
        if (!Assertion.assertNotNull(searchVal)) return null;
        for (T entity : findAll()) {
            assert entity.getDescription() != null;
            assert entity.getName() != null;
            if (entity.getName().contains(searchVal) || entity.getDescription().contains(searchVal)) {
                return entity;
            }
        }
        return null;
    }

    @NotNull
    public final List<T> getList() {

        return new ArrayList<T>(findAll());
    }

    @NotNull
    public final List<T> sortByStart() {
        getList().sort(Comparator.comparing(T::getDateStart));
        return getList();
    }

    @NotNull
    public final List<T> sortByCreation() {
        getList().sort(Comparator.comparing(T::getCreation));
        return getList();
    }

    @NotNull
    public final List<T> sortByFinish() {
        getList().sort(Comparator.comparing(T::getDateFinish));
        return getList();
    }

    @NotNull
    public final List<T> sortByStatus() {
        final Integer[] i = {null};
        final Comparator<T> statusComparator = (o1, o2) -> {
            if (o1.getStatus().equals(o2.getStatus())) i[0] = 0;
            else if (o1.getStatus().equals(Status.PLANNED)) i[0] = -1;
            else if (o2.getStatus().equals(Status.PLANNED)) i[0] = 1;
            else if (o1.getStatus().equals(Status.IN_PROCESS)) i[0] = -1;
            else if (o2.getStatus().equals(Status.IN_PROCESS)) i[0] = 1;
            return i[0];
        };
        getList().sort(statusComparator);
        return getList();
    }

    public final void editDescription(final @NotNull T entity, final @NotNull String newDescription) {
        if (!Assertion.assertNotNull(entity, newDescription)) return;
        entity.setDescription(newDescription);
    }

}
