package ru.didenko.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.service.GoalServiceInterface;
import ru.didenko.tm.entity.Project;

import java.text.ParseException;
import java.util.Date;

public interface ProjectServiceInterface extends GoalServiceInterface<Project> {

}
