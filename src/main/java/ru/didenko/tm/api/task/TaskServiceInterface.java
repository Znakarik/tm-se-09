package ru.didenko.tm.api.task;

import ru.didenko.tm.api.service.GoalServiceInterface;
import ru.didenko.tm.entity.Task;

import java.text.ParseException;
import java.util.Date;

public interface TaskServiceInterface extends GoalServiceInterface<Task> {
    void setProject(String taskName, String projectId);

}
