package ru.didenko.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.User;

import java.security.NoSuchAlgorithmException;

public interface UserServiceInterface {

    User getCurrentUser();

    void setCurrentUser(final @Nullable User currentUser);

    void createUser(final @NotNull String login, final @NotNull String pass, final @NotNull Role role) throws NoSuchAlgorithmException;

    void setCurrentUserByName(final @NotNull String name);

    User findUser(final @NotNull String login, final @NotNull String pass);
}

