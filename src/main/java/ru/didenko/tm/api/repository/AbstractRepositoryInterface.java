package ru.didenko.tm.api.repository;

import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.repository.AbstractRepository;

import java.util.Collection;

public interface AbstractRepositoryInterface<T extends AbstractEntity> {
    Collection<T> findAll();

    T findOne(String id);

    T findOneByName(String name);

    T persist(T entity);

    void remove(String id);

    void removeAll();
}
