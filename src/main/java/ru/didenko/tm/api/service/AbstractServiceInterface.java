package ru.didenko.tm.api.service;

import java.util.Collection;
import java.util.List;

public interface AbstractServiceInterface<T> {

    Collection<T> findAll();

    void removeAll();

    void showAll();

    T findOneByName(String name);

    void delete(String name);

    List<T> getList();
}
