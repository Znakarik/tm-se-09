package ru.didenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.AbstractEntity;

import java.text.ParseException;
import java.util.List;

public interface GoalServiceInterface<T extends AbstractEntity> {

    T search(final @NotNull String searchVal);

    List<T> sortByStart();

    List<T> sortByCreation();

    List<T> sortByFinish();

    List<T> sortByStatus();

    void editDescription(final @NotNull T entity, final @NotNull String newDescription);

    void createNew(final @NotNull String name, final @Nullable String description, final @Nullable String start, final @Nullable String finish) throws ParseException;
}
