package ru.didenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.api.task.TaskRepositoryInterface;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.util.Assertion;

public final class TaskRepository extends AbstractRepository<Task> implements TaskRepositoryInterface {

    @Nullable
    public Task findOneWithUserId(@NotNull String userId, @NotNull String name) {
        if (!Assertion.assertNotNull(name, userId)) return null;
        @NotNull Task goal = super.findOneByName(name);
        String goalUserId = goal.getUserId();
        assert goalUserId != null;
        if (goal.getUserId().equals(goalUserId)) {
            return goal;
        }
        return null;
    }

}
