package ru.didenko.tm.repository;

import ru.didenko.tm.api.user.UserRepositoryInterface;
import ru.didenko.tm.entity.User;

public final class UserRepository extends AbstractRepository<User> implements UserRepositoryInterface{

}
