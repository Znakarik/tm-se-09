package ru.didenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.api.project.ProjectRepositoryInterface;
import ru.didenko.tm.util.Assertion;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements ProjectRepositoryInterface {
    @Nullable
    public Project findOneWithUserId(@NotNull String userId, @NotNull String name) {
        if (!Assertion.assertNotNull(name, userId)) return null;
        @NotNull Project goal = super.findOneByName(name);
        String goalUserId = goal.getUserId();
        assert goalUserId != null;
        if (goal.getUserId().equals(goalUserId)) {
            return goal;
        }
        return null;
    }

    @Nullable
    public List<Project> findAllWithUserId(@NotNull String userId) {
        List<Project> projects = new ArrayList<>();
        if (!Assertion.assertNotNull(userId)) return null;
        for (Project project : this.findAll()) {
            String id = project.getId();
            assert id != null;
            if (id.equals(userId)) {
                projects.add(project);
            }
        }
        return projects;
    }

}
