package ru.didenko.tm.boostrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.didenko.tm.api.service.ServiceLocator;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.repository.ProjectRepository;
import ru.didenko.tm.repository.TaskRepository;
import ru.didenko.tm.repository.UserRepository;
import ru.didenko.tm.service.ProjectService;
import ru.didenko.tm.service.TaskService;
import ru.didenko.tm.service.TerminalService;
import ru.didenko.tm.service.UserService;
import ru.didenko.tm.util.Assertion;

import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.*;

import static ru.didenko.tm.constant.Role.*;

@Getter
@Setter
@NoArgsConstructor
public final class Boostrap implements ServiceLocator {

    @NotNull
    final TerminalService terminalService = new TerminalService();
    @NotNull
    final ProjectService projectService = new ProjectService(new ProjectRepository());
    @NotNull
    final TaskService taskService = new TaskService(new TaskRepository());
    @NotNull
    final UserService userService = new UserService(new UserRepository());
    @NotNull
    final Map<String, AbstractCommand<? extends AbstractCommand<?>>> commandMap = new LinkedHashMap<>();
    @NotNull
    private final Set<Class<? extends AbstractCommand>> classes1 = new Reflections("ru.didenko.tm").getSubTypesOf(ru.didenko.tm.command.AbstractCommand.class);

    @Nullable

    public final void registryOne(final @NotNull AbstractCommand command) {
        if (!Assertion.assertNotNull(command)) return;
        command.setServiceLocator(this);
        commandMap.put(command.getName(), command);
    }

    public final void registryAll() throws IllegalAccessException, InstantiationException {
        @NotNull final List<Class<? extends AbstractCommand>> comandList = new LinkedList<>(classes1);
        for (Class<? extends AbstractCommand> command : comandList) {
            if (AbstractCommand.class.isAssignableFrom(command)) {
                AbstractCommand<?> abstractCommand = command.newInstance();
                registryOne(abstractCommand);
            }
        }
    }

    public final void execute(final @NotNull String command) throws Exception {
        if (!Assertion.assertNotNull(command)) return;
        @NotNull final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand.isSecure()) {
            if (userService.getCurrentUser().getRole().displayName().equals(ADMIN.displayName())) {
                abstractCommand.execute();
            } else System.err.println("YOU DON'T HAVE PERMISSION");
        } else if (!abstractCommand.isSecure()) {
            abstractCommand.execute();
        }
    }

    public final void before() throws NoSuchAlgorithmException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, ParseException {
        userService.createUser("olya", "123", Role.USER);
        userService.createUser("totor", "123", Role.ADMIN);
        projectService.createNew("project", "", "", "");
        Project project = projectService.findOneByName("project");
        project.setUserId(userService.findOneByName("totor").getId());
        registryAll();
    }

    public final void init() {
        @NotNull final TerminalService reader = getTerminalService();
        String inputCommand = "";
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        try {
            before();
        } catch (Exception e) {
            System.err.println("SMTHG IN INITIALIZING");
        }
        do {
            try {
                // проверка на пустоту текущего пользователя
                if (userService.getCurrentUser() == null) {
                    inputCommand = "user-login";
                }
                // если есть какой-либо зарегестированный юзер
                // и есть текущий пользователь, то есть возможность вызывать команды из списка help
                else if ((userService.findAll().size() > 0) && (userService.getCurrentUser() != null)) {
                    inputCommand = reader.nextLine();
                    // если нет зарегестрированных пользователей
                } else if (userService.findAll().size() == 0) {
                    inputCommand = "user-register";
                }
                execute(inputCommand);
            } catch (Exception e) {
                System.err.println("EXCEPTION");
                e.printStackTrace();
            }
        } while (!"exit".equals(inputCommand));
    }
}