package ru.didenko.tm;

import org.reflections.Reflections;
import ru.didenko.tm.boostrap.Boostrap;
import ru.didenko.tm.command.AbstractCommand;
import ru.didenko.tm.command.ExitCommand;
import ru.didenko.tm.command.SearchCommand;
import ru.didenko.tm.command.about.AboutCommand;
import ru.didenko.tm.command.help.HelpCommand;
import ru.didenko.tm.command.project.*;
import ru.didenko.tm.command.task.*;
import ru.didenko.tm.command.user.*;

import java.util.Set;

public class App {

    public static void main(String[] args) {
        Boostrap boostrap = new Boostrap();
        boostrap.init();
    }
}
