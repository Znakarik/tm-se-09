package ru.didenko.tm.util;

public final class Assertion {
    public static boolean assertNotNull(Object... o) {
        boolean nullCheck = false;
        for (Object value : o) {
            nullCheck = value != null && !value.equals("");
        }
        return nullCheck;
    }
}

