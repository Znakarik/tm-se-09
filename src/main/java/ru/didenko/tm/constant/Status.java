package ru.didenko.tm.constant;

import org.jetbrains.annotations.NotNull;

public enum Status {
    PLANNED("PLANNED"),
    IN_PROCESS("IN PROCESS"),
    DONE("DONE");

    @NotNull
    private String status;

    Status(final @NotNull String status) {
        this.status = status;
    }

    @NotNull
    public String displayName() {
        return status;
    }
}
