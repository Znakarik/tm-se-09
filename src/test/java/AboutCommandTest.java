import com.jcabi.manifests.Manifests;
import org.junit.Test;

import java.io.*;
import java.util.Properties;

public class AboutCommandTest {
    @Test
    public void test() throws IOException {
        String developer = Manifests.read("Created-By");
        String javaVersion = Manifests.read("Originally-Created-By");

        File file = new File("buildNumber.properties");
        Properties properties = new Properties();
        properties.load(new FileInputStream(file));
        String buildNumber = properties.getProperty("buildNumber");
        System.out.format("***\nBUILD NUMBER: %s\nDEVELOPER: %s\nJAVA VERSION: %s\n***", buildNumber, developer,javaVersion);
    }
}
