import lombok.SneakyThrows;
import org.junit.Test;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.repository.UserRepository;
import ru.didenko.tm.service.UserService;

public class FindOneUserRepoTest {
    @SneakyThrows
    @Test
    public void test() {
        UserRepository userRepository = new UserRepository();
        UserService userService = new UserService(userRepository);

        userService.createUser("test","111",Role.USER);
        userService.findAll().forEach(user -> System.out.println(user.getName()));
        userService.showAll();
    }
}
