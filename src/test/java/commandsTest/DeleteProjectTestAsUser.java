package commandsTest;

import org.junit.Test;
import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public class DeleteProjectTestAsUser extends AbstractTest {

    @Test
    public void test() throws NoSuchAlgorithmException, ParseException {
        User user = createAdmin();
        userService.setCurrentUser(user);

        Project project = projectCreate();
        Task task = taskCreate();
        taskService.setProject(task.getName(),project.getName());

        if (userService.getCurrentUser().getRole() == Role.ADMIN) {
            projectService.delete(project.getName());
            System.out.println("[PROJECT DELETED]");
        } else System.out.println("DON'T HAVE PERMISSION");
    }
}
