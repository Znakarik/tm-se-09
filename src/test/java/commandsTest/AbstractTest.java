package commandsTest;

import ru.didenko.tm.constant.Role;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;
import ru.didenko.tm.entity.User;
import ru.didenko.tm.repository.ProjectRepository;
import ru.didenko.tm.repository.TaskRepository;
import ru.didenko.tm.repository.UserRepository;
import ru.didenko.tm.service.ProjectService;
import ru.didenko.tm.service.TaskService;
import ru.didenko.tm.service.UserService;
import ru.didenko.tm.util.DateParser;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public class AbstractTest {
    UserService userService = new UserService(new UserRepository());
    ProjectService projectService = new ProjectService(new ProjectRepository());
    TaskService taskService = new TaskService(new TaskRepository());

    public User createUser() throws NoSuchAlgorithmException {
        userService.createUser("user", "111", Role.USER);
        return userService.findOneByName("user");
    }

    public User createAdmin() throws NoSuchAlgorithmException {
        userService.createUser("admin", "111", Role.ADMIN);
        return userService.findOneByName("admin");
    }

    public Project projectCreate() throws ParseException {
        projectService.createNew("project1", "about", "", "");
        return projectService.findOneByName("project1");
    }

    public Task taskCreate() throws ParseException {
        taskService.createNew("task1", "about", "", "");
        return taskService.findOneByName("task1");
    }
}
