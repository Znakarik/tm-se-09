package jaxb.myTrying.entry;

import jaxb.myTrying.entry.BaseObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "man")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
public class Man extends BaseObject {

    public Man(String name, int age) {
        super(name, age);
    }

    @Override
    public String toString() {
        return "Man{} " + super.toString();
    }
}
