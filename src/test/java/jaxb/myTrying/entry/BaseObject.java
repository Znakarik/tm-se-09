package jaxb.myTrying.entry;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;

@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@XmlSeeAlso({Man.class, Woman.class})
public abstract class BaseObject {
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "age")
    private int age;

    public BaseObject(String name,int age){
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "BaseObject{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
