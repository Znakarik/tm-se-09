package jaxb.myTrying.entry;

import jaxb.myTrying.entry.BaseObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "woman")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
public class Woman extends BaseObject {
    @XmlElement(name = "money")
    private int money;

    public Woman(String name, int age, int money) {
        super(name,age);
        this.money = money;
    }
}
