package jaxb.myTrying.repository;

import jaxb.myTrying.entry.BaseObject;
import jaxb.myTrying.entry.Man;
import jaxb.myTrying.entry.Woman;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "people")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@Data
public class People {

    @XmlElement(name = "human")
    private List<BaseObject> people = new ArrayList<>();

    public List<BaseObject> createPeople() {
        Woman woman = new Woman("Луиза", 23, 100);
        Man man = new Man("Джек", 30);
        people.add(woman);
        people.add(man);
        return people;
    }
}
