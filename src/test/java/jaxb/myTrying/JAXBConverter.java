package jaxb.myTrying;

import jaxb.myTrying.entry.BaseObject;
import jaxb.myTrying.entry.Man;
import jaxb.myTrying.entry.Woman;
import jaxb.myTrying.repository.People;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringWriter;
import java.util.List;

public class JAXBConverter {
    StringWriter writer = new StringWriter();

    public void marshallObject(BaseObject object) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(object.getClass());

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshaller.marshal(object, writer);

        String result = writer.toString();
        System.out.println(result);
    }

    public void marshallObjects(People object) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(object.getClass());

        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshaller.marshal(object, writer);

        String result = writer.toString();
        System.out.println(result);

    }

    public void unmarshalObjectFromXML(String path) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(BaseObject.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        BaseObject o = (BaseObject) unmarshaller.unmarshal(new File(path));
        System.out.format("NAME: %s\nAGE: %s", o.getName(), o.getAge());
    }

    public void unmarshalObjectsFromXML(String path) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(BaseObject.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        List<BaseObject> o = (List<BaseObject>) unmarshaller.unmarshal(new File(path));

    }

    public static void main(String[] args) throws JAXBException {
        JAXBConverter converter = new JAXBConverter();
        People people = new People();
        Woman woman = new Woman("Луиза", 23, 100);
        Man man = new Man("Джек", 30);

        people.createPeople();

        converter.marshallObjects(people);
//        converter.marshallObject(man);
        converter.unmarshalObjectFromXML("/Users/o/IdeaProjects/tm-se-09/src/test/java/jaxb/object.xml");
//        converter.unmarshalObjects("/Users/o/IdeaProjects/tm-se-09/src/test/java/jaxb/objects.xml");
//        converter.
    }

    @Test
    public void test() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(People.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        People people = (People) unmarshaller.unmarshal(new File("/Users/o/IdeaProjects/tm-se-09/src/test/java/jaxb/myTrying/source/objects.xml"));

        for (BaseObject human : people.getPeople()) {
            System.out.println(human.getName());
            System.out.println(human.getAge());
            System.out.println(human.hashCode());
        }
    }
}