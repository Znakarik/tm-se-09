package jaxb;

import lombok.NoArgsConstructor;
import ru.didenko.tm.entity.AbstractEntity;
import ru.didenko.tm.entity.Project;
import ru.didenko.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "entities")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
public class ProjectAndTaskWithJAXBTest {

    @XmlElement(name = "entity")
     public List<AbstractEntity> list = new ArrayList<>();

    public static void main(String[] args) throws JAXBException {
        Project project = new Project();
        project.setName("Olga's project");
        project.setDescription("about Olga");
        Task task = new Task();
        task.setProjectId(project.getId());

        ProjectAndTaskWithJAXBTest test = new ProjectAndTaskWithJAXBTest();
        test.list.add(project);
        test.list.add(task);

        JAXBContext context = JAXBContext.newInstance(ProjectAndTaskWithJAXBTest.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(test, System.out);

//        String result = writer.toString();

    }

}
