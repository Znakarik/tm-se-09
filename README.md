# Developer info
Didenko Olga - znakarik@icloud.com

# Build Maven project
```
mvn clean install 
```

# Run taskmanager in terminal 
```
java -jar taskmanager-1.0.0.jar
```

# System requirement
 
* Java - openjdk version "13.0.2"

* Mac OS Mojave 10.14.5
 